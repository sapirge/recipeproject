<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\RecipeAsset;
use app\models\Category;
use yii\helpers\ArrayHelper;
RecipeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
        <?php //echo \Yii::getAlias('@webroot');exit;?>
      <a class="navbar-brand" href="<?php echo Yii::$app->homeUrl?>">
        <!-- <img src="<?php echo $this->theme->baseUrl;?>/images/logo.png"> -->
        Logo
      </a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="<?php echo Yii::$app->homeUrl?>">Home</a></li>
      <!-- <li><a href="/page1">Page 1</a></li> -->
      <!-- <li><a href="#">Page 2</a></li> -->
    </ul>
    <form class="navbar-form navbar-left" method="post" action="<?php echo Yii::$app->homeUrl?>recipe/searchrecipe">
      <div class="form-group">
        <input type="text" class="form-control" name="searchterm" placeholder="Search">
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <ul class="nav navbar-nav navbar-right">
        <?php if(Yii::$app->user->isGuest):?>
      <li><a href="<?php echo Yii::$app->homeUrl?>users/signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <?php endif;?>
      <?php if(Yii::$app->user->isGuest):?>
      <li><a href="<?php echo Yii::$app->homeUrl?>users/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      <?php else:?>
        <li>
            <?php echo Html::beginForm(['/users/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
            ?>
            <!-- <a href="<?php echo Yii::$app->homeUrl;?>site/logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a> -->

        </li>
        <li><a href="<?php echo Yii::$app->homeUrl?>recipe/create"> Add Recipe</a></li>
      <?php endif;?>
    </ul>
  </div>
</nav>
<?= Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
    <div class="banner container-fluid main-content">
        <div class="w3l_banner_nav_lefts col-md-3">
            <nav class="navbar nav_bottom">
             <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header nav_2">
                  <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
               </div> 
               <!-- Collect the nav links, forms, and other content for toggling -->
               <?php
               $category = Category::find()
                        ->all();
                $categories=ArrayHelper::map($category,'id','name');
                //print_r($category);exit;
               ?>
                <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                    <ul class="nav navbar-nav nav_1">
                        <?php foreach($category as $cat):?>
                            <li><a href="<?php echo Yii::$app->homeUrl?>recipe/recipebycategory?id=<?php echo $cat->id?>"><?php echo $cat->name?></a></li>
                        <?php endforeach;?>
                    </ul>
                 </div><!-- /.navbar-collapse -->
            </nav>
        </div>
        <div class="w3l_banner_nav_rights col-md-9">
                
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
        <div class="clearfix"></div>
    </div>
    
</div>

<!-- footer -->
    <div class="footer">
        <div class="container">
            <!-- <div class="col-md-3 w3_footer_grid">
                <h3>information</h3>
                <ul class="w3_footer_grid_list">
                    <li><a href="events.html">Events</a></li>
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="products.html">Best Deals</a></li>
                    <li><a href="services.html">Services</a></li>
                    <li><a href="short-codes.html">Short Codes</a></li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>policy info</h3>
                <ul class="w3_footer_grid_list">
                    <li><a href="faqs.html">FAQ</a></li>
                    <li><a href="privacy.html">privacy policy</a></li>
                    <li><a href="privacy.html">terms of use</a></li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>what in stores</h3>
                <ul class="w3_footer_grid_list">
                    <li><a href="pet.html">Pet Food</a></li>
                    <li><a href="frozen.html">Frozen Snacks</a></li>
                    <li><a href="kitchen.html">Kitchen</a></li>
                    <li><a href="products.html">Branded Foods</a></li>
                    <li><a href="household.html">Households</a></li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>twitter posts</h3>
                <ul class="w3_footer_grid_list1">
                    <li><label class="fa fa-twitter" aria-hidden="true"></label><i>01 day ago</i><span>Non numquam <a href="#">http://sd.ds/13jklf#</a>
                        eius modi tempora incidunt ut labore et
                        <a href="#">http://sd.ds/1389kjklf#</a>quo nulla.</span></li>
                    <li><label class="fa fa-twitter" aria-hidden="true"></label><i>02 day ago</i><span>Con numquam <a href="#">http://fd.uf/56hfg#</a>
                        eius modi tempora incidunt ut labore et
                        <a href="#">http://fd.uf/56hfg#</a>quo nulla.</span></li>
                </ul>
            </div> -->
            <div class="clearfix"> </div>
            <!-- <div class="agile_footer_grids">
                <div class="col-md-3 w3_footer_grid agile_footer_grids_w3_footer">
                    <div class="w3_footer_grid_bottom">
                        <h4>100% secure payments</h4>
                        <img src="images/card.png" alt=" " class="img-responsive" />
                    </div>
                </div>
                <div class="col-md-3 w3_footer_grid agile_footer_grids_w3_footer">
                    <div class="w3_footer_grid_bottom">
                        <h5>connect with us</h5>
                        <ul class="agileits_social_icons">
                            <li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div> -->
            <div class="wthree_footer_copy">
                <p>© 2016 Recipe. All rights reserved</a></p>
            </div>
        </div>
    </div>
<!-- //footer -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
