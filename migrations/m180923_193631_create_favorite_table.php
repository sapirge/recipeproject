<?php

use yii\db\Migration;

/**
 * Handles the creation of table `favorite`.
 */
class m180923_193631_create_favorite_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('favorite', [
            'id' => $this->primaryKey(),
			'user_id' =>  $this->integer()->notNull(), 
			'recipe_id' =>  $this->integer()->notNull(), 
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('favorite');
    }
}
