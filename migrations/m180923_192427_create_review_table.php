<?php

use yii\db\Migration;

/**
 * Handles the creation of table `review`.
 */
class m180923_192427_create_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('review', [
				'id' => $this->primaryKey(),
				'rating' => $this->integer()->notNull(), 
				'feedback' => $this->text()->notNull(),
		
        ]);
		$this->addForeignKey(
            'recipe_id',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('review');
    }
}
