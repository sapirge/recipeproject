<?php

/* @var $this yii\web\View */
use app\models\Usermodel;
$this->title = 'Recipe';
?>  
<section class="slider">
    <div class="flexslider">
        
    <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 1000%; transition-duration: 0.6s; transform: translate3d(-2158px, 0px, 0px);"><li class="clone" style="width: 1079px; float: left; display: block;">
                <div class="w3l_banner_nav_right_banner2">
                    <h3>upto <i>50%</i> off.</h3>
                    <div class="more">
                        <a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                    </div>
                </div>
            </li>
            <li style="width: 1079px; float: left; display: block;" class="">
                <div class="w3l_banner_nav_right_banner">
                    <h3>Make your <span>food</span> with Spicy.</h3>
                    <div class="more">
                        <a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                    </div>
                </div>
            </li>
            <li class="flex-active-slide" style="width: 1079px; float: left; display: block;">
                <div class="w3l_banner_nav_right_banner1">
                    <h3>Make your <span>food</span> with Spicy.</h3>
                    <div class="more">
                        <a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                    </div>
                </div>
            </li>
            <li class="" style="width: 1079px; float: left; display: block;">
                <div class="w3l_banner_nav_right_banner2">
                    <h3>upto <i>50%</i> off.</h3>
                    <div class="more">
                        <a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                    </div>
                </div>
            </li>
        <li style="width: 1079px; float: left; display: block;" class="clone">
                <div class="w3l_banner_nav_right_banner">
                    <h3>Make your <span>food</span> with Spicy.</h3>
                    <div class="more">
                        <a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                    </div>
                </div>
            </li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a class="">1</a></li><li><a class="flex-active">2</a></li><li><a class="">3</a></li></ol><ul class="flex-direction-nav"><li><a class="flex-prev" href="#">Previous</a></li><li><a class="flex-next" href="#">Next</a></li></ul></div>
</section>
<div class="top-brands">
    <div class="">
        <h3>Popular Recipe</h3>
        <div class="agile_top_brands_grids row">
            <div class="col-md-3 top_brand_left">
                <div class="hover14 column">
                    <div class="agile_top_brand_left_grid">
                        
                        <div class="agile_top_brand_left_grid1">
                            <figure>
                                <div class="snipcart-item block">
                                    <div class="snipcart-thumb">
                                        <a href="single.html"><img title=" " alt=" " src="images/1.png"></a>        
                                        <p>fortune sunflower oil</p>
                                        <h4>
                                            <span>By: xyz</span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>

                                        </h4>
                                    </div>
                                    <div class="snipcart-details top_brand_home_details">
                                        
                                    <a href="#" class="view-more">View</a>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="top-brands">
    <div class="">
        <h3>New Recipe</h3>
        <div class="agile_top_brands_grids row">
            <?php foreach($newrecipe as $recipe):?>
                <?php $user = Usermodel::find()
                ->where(['>=', 'id', $recipe['posted_by']])
            ->all();//print_r($user);exit;
                 ?>
            <div class="col-md-3 top_brand_left">
                <div class="hover14 column">
                    <div class="agile_top_brand_left_grid">
                        <div class="agile_top_brand_left_grid1">
                            <figure>
                                <div class="snipcart-item block">
                                    <div class="snipcart-thumb">
                                        <a href="<?php echo Yii::$app->homeUrl?>recipe/view?id=<?php echo base64_encode($recipe['id'])?>"><img title=" " alt=" " src="<?php echo Yii::$app->homeUrl?>uploads/<?php echo $recipe['image'];?>" height="110"></a>        
                                        <p><?php echo $recipe['name'];?></p>
                                        <h4>
                                            <div class="postby">
                                                <span><span class="fa fa-user"></span> <?php echo $user[0]['first_name'].' '.$user[0]['last_name']?></span>
                                            </div>
                                            <div class="reviewDiv">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </div>
                                        </h4>
                                    </div>
                                    <div class="snipcart-details top_brand_home_details">
                                        
                                    <a href="<?php echo Yii::$app->homeUrl?>recipe/view?id=<?php echo base64_encode($recipe['id'])?>" class="view-more">View</a>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>