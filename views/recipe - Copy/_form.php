<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;

use app\models\Category;
/* @var $this yii\web\View */
/* @var $model app\models\Recipe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recipe-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?> -->
    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>
    <?= $form->field($model, 'ingredients')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>
    <!-- <?= $form->field($model, 'ingredients')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'image')->textarea(['rows' => 6]) ?> -->
    <?= $form->field($model, 'file')->widget(FileInput::classname(), [
              'options' => ['accept' => 'image/*'],
               'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],'showUpload' => false,],
          ]);   ?>
    <!-- <?= $form->field($model, 'category')->textInput() ?> -->
    <?php
        $category = Category::find()
            ->all();
        $listData=ArrayHelper::map($category,'id','name');
        echo $form->field($model, 'category')->dropDownList(
            $listData,
            ['prompt'=>'Select...']
        );
    ?>
    <?= $form->field($model, 'cooking_time')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'posted_by')->textInput() ?> -->
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
